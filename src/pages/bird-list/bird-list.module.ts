import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BirdListPage } from './bird-list';

@NgModule({
  declarations: [
    BirdListPage,
  ],
  imports: [
    IonicPageModule.forChild(BirdListPage),
  ],
})
export class BirdListPageModule {}
